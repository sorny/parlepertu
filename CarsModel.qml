import QtQuick 2.0

ListModel {
    ListElement {
        name: "Audi"
        origin: "Logos/audi.png"
    }
    ListElement {
        name: "Bmw"
        origin: "Logos/bmw.png"
    }
    ListElement {
        name: "Fiat"
        origin: "Logos/fiat.jpg"
    }
    ListElement {
        name: "Mercedes"
        origin: "Logos/mercedes.jpg"
    }
    ListElement {
        name: "Ford"
        origin: "Logos/ford.jpg"
    }
    ListElement {
        name: "Seat"
        origin: "Logos/seat.png"
    }
    ListElement {
        name: "Volvo"
        origin: "Logos/volvo.jpg"
    }
}

